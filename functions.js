function palindrom(str) {
    return str === str.split('').reverse().join('')
}

function alphabeta(n) {
    let result = []

    for(let i = 1; i <= n; ++i) {
        if(i % 15 === 0) {
            result.push('alphabeta')
            continue
        }

        if(i % 5 === 0) {
            result.push('beta')
            continue
        }

        if(i % 3 === 0) {
            result.push('alpha')
            continue
        }

        result.push(i)
    }

    return result
}

function countOfVowels(str) {
    return (str.match(/[aeoyui]/gi) || []).length
}

module.exports.palindrom = palindrom
module.exports.alphabeta = alphabeta
module.exports.countOfVowels = countOfVowels 