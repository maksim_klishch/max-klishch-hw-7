const {palindrom, countOfVowels, alphabeta} = require('./functions')
const publicIp = require('public-ip');
const fetch = require('node-fetch');

describe('palindrom', () => {
    test('palindrom', () => {
        expect(palindrom('owo')).toBeTruthy()
        expect(palindrom('hello')).toBeFalsy()
        expect(palindrom('racecar')).toBeTruthy()
        expect(palindrom('test test')).toBeFalsy()
        expect(palindrom('hwwh')).toBeTruthy()
    })
})

describe('alphabeta', () => {
    test('alphabeta', () => {
        expect(alphabeta(5)).toContain('beta')
        expect(alphabeta(3)).toContain('alpha')
        expect(alphabeta(15)).toContain('alphabeta')
        expect(alphabeta(2)).not.toContain('beta')
        expect(alphabeta(30)).toContain('alphabeta')
    })
})

describe('count of vowels', () => {
    test('count of vowels', () => {
        expect(countOfVowels('owo')).toBe(2)
        expect(countOfVowels('hello')).toBe(2)
        expect(countOfVowels('racecar')).toBe(3)
        expect(countOfVowels('test test')).toBe(2)
        expect(countOfVowels('hwwh')).toBe(0)
    })
})

describe('ip check', () => {
    async function httpbin() {
        return await fetch("https://httpbin.org/get").then(res => res.json())
    }
    
    test('ip check', async () => {
        await expect(httpbin()).resolves.toHaveProperty('origin', await publicIp.v4())
    })
})